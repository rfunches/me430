'''
@file task_user.py

This file is used to communicate with the computer through serial communication, initialize
all objects, and loop the controller run() function for an appropriate time interval. This file takes
input reference velocity and Kp from the computer and returns a list for velocity and time.
'''
import pyb
from pyb import UART
import utime
import array
from controller import controller
from EncoderLab6 import Encoder
from MotorDriver import MotorDriver
from LimitDriver import LimitSwitch

#Initialize serial communication

myuart=pyb.UART(2) #initialize UART for communication with computer

pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

t2ch1.pulse_width_percent(100)

#waiting for Kp
n=0 #loop control variable
while(n==0): #runs while n==0, changed from 0 when Kp received
    if myuart.any() != 0: #if there's anything from the computer
        Kp = myuart.read().decode('ascii') #read the line
        Kp=float(Kp)
        n=1
        
t2ch1.pulse_width_percent(0)
        
#interval passed over from front end
n=0 #loop control variable
while(n==0): #runs while n==0, changed from 0 when Kp received
    if myuart.any() != 0: #if there's anything from the computer
        interval = myuart.read().decode('ascii') #read the line,Interval at which FSM will run tasks in ms
        interval=float(interval)
        n=1



t2ch1.pulse_width_percent(100)


#Encoder Stuff
period=0xffff #Period of timer

tim=pyb.Timer(4) #Declare timer 4
tim.init(prescaler=0,period=0xFFFF) #Initialize timer 4 with prescale and period
tim.channel(1,pin=pyb.Pin.cpu.B6,mode=pyb.Timer.ENC_AB) #channel 1 of timer 4 to pin B6
tim.channel(2,pin=pyb.Pin.cpu.B7,mode=pyb.Timer.ENC_AB) #chennel 2 of timer 4 to pin B7

CPR=1000 #CPR of motor
enc=Encoder(tim,period,interval,CPR) #declare encoder object, pass timer,period, interval




#Motor stuff
pinA15=pyb.Pin(pyb.Pin.cpu.A15,pyb.Pin.OUT_PP) #set nSleep pin to be an output pin
pinB4=pyb.Pin(pyb.Pin.cpu.B4) #initialize pin relating to IN1 for motor 1
tim3=pyb.Timer(3,freq=20000) #initialize timer 3 that works with all pins
pinB5=pyb.Pin(pyb.Pin.cpu.B5) #initialize pin relating to IN2 for motor 1

mot=MotorDriver(pinA15,pinB4,pinB5,tim3,1)#initialize motor objects by passing nSLEEP pin, two enable pins, and motor number to initialize channel
mot.enable() #enable motor

#Limit switch stuff
sensorPin=pyb.Pin(pyb.Pin.cpu.A0)
sensor=LimitSwitch(sensorPin)

#closed loop control/controller

controller=controller(Kp,omegaref[0],mot,enc) #controller object initialized, pass Kp, omegaref, interval, mot,enc,cloop


#Data collection
TimeData=array.array('i',[]) #list to store time values in ms
SpeedData=array.array('f',[])#list to store velocity data in rpm
PosData=array.array('f',[])

#timing
start_time = utime.ticks_ms() #time for data collection to start
refcontrol=0

while(refcontrol<len(tref)): #motor reaching steady state takes under 1 second, so run control system for 1000ms
    curr_time=utime.ticks_ms() #current time 
    if curr_time-start_time>=(tref[refcontrol]): #if current time is past the designated next time
        controller.setOmegaRef(omegaref[refcontrol]*20) #reference values scaled up by 20 so that my motor can run, hire operating speed required
        speed=controller.run() #current speed calculated by encoder inside controller
        newpos=enc.get_positionDeg()
        PosData.append(newpos)
        time=curr_time-start_time #current change in time relative to start time
        TimeData.append(time) #append time to time list
        SpeedData.append(speed) #append velocity to velocity list
        
        refcontrol=refcontrol+1
        
mot.disable() #disable motor after data returned to ensure safety        
    


myuart.write('{:}\r\n'.format(len(SpeedData))) #passes length of list back to main in order to control loop
for n in range(len(SpeedData)): #loops through entirety of time and velocity lists
        myuart.write('{:},{:},{:}\r\n'.format(TimeData[n],SpeedData[n],PosData[n])) #write values of time and velocity seperated by a comma at current index
                                      
        
        
        
        
        
        
        
        
        
        
        
        
        
# tref=[]
# omegaref=[]
# utime.sleep(5) #pause for 3 seconds before reading from UART based on passed CSV

# vlength=myuart.readline().decode('ascii') #obtain length of velocity list
# vlength=vlength.strip()
# vlength=int(vlength) #velocity list length cast as int, used for loop control

# t2ch1.pulse_width_percent(100)
# myuart.write('{:}'.format(vlength).encode('ascii')) #write values of time and velocity seperated by a comma at current index
# for n in range(1,vlength): #loop through entire list
#     t2ch1.pulse_width_percent(0)
#     line_string = myuart.readline().decode('ascii') #read the line which has time and velocity comma seperated
#     line_string=(line_string.strip()) #strips all special characters from String, such as \r and \n
#     t2ch1.pulse_width_percent(100) 
#     line_list = line_string.split(',')		#split string at comma, 0th entry is time, 1st entry is velocity
#     t2ch1.pulse_width_percent(0) 
#     tref.append(float(line_list[0]))				#append 0th entry to t list	
#     omegaref.append(float(line_list[1]))		#append 1st entry to velocity list
#     t2ch1.pulse_width_percent(100)
    
   



