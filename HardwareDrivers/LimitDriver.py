''' 
@file LimitDriver.py

'''

import pyb
import utime
from pyb import Pin
from pyb import ADC

class LimitSwitch:
    '''
    @brief  This Class implements a limit switch'''
    
    def __init__(self,sensor):
        '''
        @brief  Makes limit switch object
        @param sensor Pin object connected to limit switch
        '''
        self.sensor=sensor
        self.sensor.init(mode=Pin.ANALOG)
        self.reader=ADC(self.sensor)
        
    def read(self):
        '''
        @brief This function reads from the limit switch and returns True if engaged.
        @details Returns True if limit switch is engaged and False if no obstruction is detected.
        '''
        reading=self.reader.read()
        if reading>2500:
            return True
        return False
        
        
    

if __name__=="__main__":
    
    sensorPin=Pin(Pin.cpu.A0)
    sensor=LimitSwitch(sensorPin)
    
    while True:
        sensor.read()
    
    