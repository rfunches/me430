'''
@file shares.py
@param delta  The int value of delta is passed here by Encoder.py to return to users in userface.py
@param position The int value of delta is passed here by Encoder.py to return to users in userface.py
@param clearpos  This int is only ever equal to 0 or 1, with a 1 signifying to encoder.py that position needs to be reset to 0

This file contains shared variables between userface.py and Encoder.py
'''

delta=None

position=None

clearpos=0