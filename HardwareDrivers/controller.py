'''
@file      controller.py
@brief     This file contains the controller class used to institute the ball balancing table control system.
@details   This file contains a controller object that acts as a closed loop system. The working principle of the object
is that it takes in inputs from the touchpad and encoders, determines the duty cycle to apply to each motor, and applies 
the duty cycle to each motor. Upon construction, a controller object is passed the relevant gain vectors, reference position,
motor, and encoder for each of the two degrees of freedom, as well as the relevant touchpad object and interval at which the run method
is to run. The run() method is run at the rate specified by interval in main.py.

@author    Hunter Brooks and Ryan Funchess
@date      March 15, 2021
'''

import pyb
import utime
import math
import shares
pi = math.pi

class controller:
    '''
    @brief  This Class implements a closed loop control algorithm
    
    
    #option for P, PI, or PD control
    #pass 
    '''
    
    sat_limit=50
    
    #m traveled per revolution
    pitch=0.005 
    
    def __init__(self,Kp,posref,mot,enc,LimitSwitchThrust,LimitSwitchFree,lidswitch):
        '''
        @brief  Makes controller object
        @param Kp  Float that designates the position controller gain for the control system (V/rpm)
        @param omegaref  Float that designates reference velocity of motor (rpm)
        @param mot   Motor object that rotates output shaft proportional to the duty cycle of the motor
        @param enc  Encoder object that is used to track the position of the output shaft
        @param cloop  Closed loop object that is used to determine the voltage to apply to the motor given the current speed of the motor        
        '''
        ##Float that designates the proportional controller gain for the control system (V/rpm)
        self.Kp=Kp #passed Kp (V/m)
        
        # ##Float that designates the integral controller for the control system
        # self.Ki=Ki
        
        # ##Float that designates the derivative controller for the control system
        # self.Kd=Kd
        
        ##Float that designates reference velocity of motor (rpm)
        self.posref=posref #designated reference velocity in rpm
        
        ##Motor object that rotates output shaft proportional to the duty cycle of the motor
        self.mot=mot
        
        ##Encoder object that is used to track the position of the output shaft
        self.enc=enc
        
        ##Limit switch closest to thrust bearing
        self.ls1=LimitSwitchThrust
        
        ##Limit switch closest to free face
        self.ls2=LimitSwitchFree
        
        self.lidswitch=lidswitch
    


    def startup(self): #runs first, zeros table to limit switch
        #bring table to limit switch and then stop
        #zero encoder shares.clearpos=1
        #while the left limit switch is not reading true:
        #   set motor duty cycle to-50
        #set motor duty to 0 after breaks through loop
        #zero the encoder
        #await
        
        while(not(self.ls1.read())):
            self.mot.set_duty(-50)
        self.mot.set_duty(0)
        shares.clearpos=1
        
        
        
    def run(self):
        #write to UART
        '''
        @brief  This method runs the closed loop control system
        @details This method is run in a While(true) loop in main.py. This method
        works cooperatively in that it first checks if it is time to run based on the passed interval, and passes otherwise.
        If the current time is equal to or past the denoted next time to run, the position and velocity of the ball as well as the 
        angle and angular velocity of the table are found in both degrees of freedom. These values are then stored in two vectors that contain
        all relevant information for each degree of freedom. These vectors are then "corrected" by subtracting our observed position
        by our desired position to obtain our error signal. The corrected vectors for each degree of freedom are then
        passed to a control method that computes the duty cycle to apply to the motor based on our numerical MATLAB analysis. These duty cycles
        are finally applied to each motor respectively and the next time for this method to run is specified based on the interval attribute.
        '''
        #if either limit switch isnt engaged
            #update encoder
            #get position and speed
            #obtain voltage to apply to motor(from control system)
            #set motor duty cycle
            #return time, position, velocity (write to uart in main method)
            
        #else if the limit switches are engaged
            #disable motor
            #set duty cycle to 0
        if(not(self.ls1.read() and self.ls2.read()) and self.lidswitch.isclosed()):
            self.enc.update() #updates encoder to find current delta 
            posact=self.enc.get_position()
            omega=self.enc.get_speed() #obtain current velocity based on delta and interval time
            level=self.controlVoltage(posact,omega) #obtain level to apply to motor by passing current speed to cloop
            self.mot.set_duty(level) #set duty cycle on motor input voltage based on output from cloop
            return [posact,omega,level] #return omega to task_user to append onto velocity list
        
        else:
            self.mot.disable()
            self.mot.set_duty(0)
        
        
        
        
        
        
        
        
        
        
        # curr_time=utime.ticks_ms() #current time 
        # if utime.ticks_diff(curr_time,self.next_time)>0: #if current time is past the designated next time
        #     positions = self.scan_positions() #obtain x, xdot, y, ydot
        #     angles = self.scan_angles()  #obtain theta x, theta y, theta dot x, theta dot y for the table
        #     x = [positions[1], angles[3], positions[0], angles[1]] #store xdot, theta ydot, x, theta in x, represents x DOF
        #     y = [positions[3], angles[2], positions[2], angles[0]] #store ydot, theta xdot, y, theta in y, represents y DOF
            
        #     x = self.correctDOF1(x) #subtract measured values from reference x position to get error signal
        #     y = self.correctDOF2(y) #subtract measured values from reference y position to get error signal
        #     Dx = self.controlDOF1(x) #obtain duty cycle to apply to x motor based on MATLAB analysis/tuning
        #     Dy = self.controlDOF2(y) #obtain duty cycle to apply to y motor based on MATLAB analysis/tuning
            
        #     self.motx.set_duty(Dx) #Apply output duty cycle to motor x
        #     self.moty.set_duty(Dy) #Apply output duty cycle to motor y
        
        #     self.next_time = utime.ticks_add(self.next_time, self.interval) #set the next time for the task to actually run
        
            
        def set_pos(self,newpos):
            self.posref=newpos
            
        def getPosRef(self):
            '''
            @brief This function returns the current value of omegaref held in this class.
            '''
            return self.posref
        
        def controlVoltage(self,pos,vel):
            #convert angle to distance with pitch
            angle=self.enc.get_positionRot()
            posact=angle*self.pitch
            Vp=self.Kp*(self.posref-posact)
            return Vp
            
           
        
            
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        



            
    